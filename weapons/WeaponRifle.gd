extends Weapon
class_name WeaponRifle

func _ready():
	max_ammo = 500
	ammo = max_ammo
	cooldown = 0.02
	spread = 2

	$first_person_rifle/AnimationPlayer.set_blend_time("Walk","Idle",0.1)
	$first_person_rifle/AnimationPlayer.set_blend_time("Walk","Fire",0.1)
	
	$first_person_rifle/AnimationPlayer.set_blend_time("Idle","Walk",0.1)
	$first_person_rifle/AnimationPlayer.set_blend_time("Idle","Fire",0.1)
	
	$first_person_rifle/AnimationPlayer.set_blend_time("Fire","Idle",0.1)
	$first_person_rifle/AnimationPlayer.set_blend_time("Fire","Walk",0.1)

	var _a = EventBus.connect("update_walking",self,"_onUpdateWalking")

func _onUpdateWalking(isWalking:bool):
	if !is_firing:
		if isWalking:
			$first_person_rifle/AnimationPlayer.play("Walk")
		else:
			$first_person_rifle/AnimationPlayer.play("Idle")

func _onFire():
	#animate
	$first_person_rifle/AnimationPlayer.play("Fire")
	pass
