extends KinematicBody

export var move_speed = 100.0
export var mouseSensitivity = 0.5
export var grav = Vector3(0,-1,0)

var velocity = Vector3();

var mouseSpeed = Vector2(0,0)

var sparkScene = preload("res://prefabs/sparks.tscn")

onready var weapons:WeaponManager = $Camera/WeaponManager

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

	# $Camera/first_person_rifle/AnimationPlayer.set_blend_time("Idle", "Walk", 0.2)
	# $Camera/first_person_rifle/AnimationPlayer.set_blend_time("Walk", "Idle", 0.2)
	# $Camera/first_person_rifle/AnimationPlayer.set_blend_time("Fire", "Idle", 0.1)
	# $Camera/first_person_rifle/AnimationPlayer.set_blend_time("Fire", "Walk", 0.1)
	pass

func _input(event):
	if event is InputEventMouseMotion:
		mouseSpeed = event.relative

func _physics_process(delta):
	velocity += grav*delta;

	var move_vec = Vector3()
	
	if Input.is_action_pressed("move_forward"):
		move_vec -= transform.basis.z
	
	if Input.is_action_pressed("move_backward"):
		move_vec += transform.basis.z

	if Input.is_action_pressed("move_left"):
		move_vec -= transform.basis.x

	if Input.is_action_pressed("move_right"):
		move_vec += transform.basis.x

	if Input.is_action_pressed("ui_cancel"):
		get_tree().quit()

	if Input.is_mouse_button_pressed(BUTTON_LEFT):
		# $Camera/first_person_rifle/AnimationPlayer.play("Fire")
		weapons.fire($Camera/WeaponManager/RayCast)
	else:
		weapons.cancelFire()

	EventBus.emit_signal("update_walking", move_vec.length() > 0.1)

	move_vec = move_vec.normalized() * move_speed * 100 * delta
	move_vec += velocity

	velocity.y = move_and_slide(move_vec).y

	#Rotation stuff
	mouseSpeed *= 0.5
	var rot = mouseSpeed * delta * mouseSensitivity
	
	rotation.y -= rot.x
	$Camera.rotation.x -= rot.y
	
	pass

func fire():
	if $Camera/RayCast.is_colliding():
		var collider = $Camera/RayCast.get_collider()
		var position = $Camera/RayCast.get_collision_point()
		
		#Spawn sparks
		var sparkRoot = sparkScene.instance()
		sparkRoot.global_transform.origin = position
		get_tree().root.add_child(sparkRoot)

		#Apply a force if we can
		if collider is RigidBody:
			var localPosition = collider.to_local(position)
			var normal = $Camera/RayCast.get_collision_normal()
			collider.apply_impulse(localPosition, -normal)
			
	pass
