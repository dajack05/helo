
extends Spatial
class_name WeaponManager

export(int) var selection:int = 0
export(Array, PackedScene) var weapons

func _ready():
	for i in weapons.size():
		if weapons[i]:
			weapons[i] = weapons[i].instance()
			add_child(weapons[i])

func fire(ray:RayCast):
	(weapons[selection] as Weapon).fire(ray)

func cancelFire():
	(weapons[selection] as Weapon).cancelFire()
