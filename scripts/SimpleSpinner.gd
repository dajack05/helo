extends Spatial

export var speed:float = 1.0
export var rotate_dir:Vector3 = Vector3.UP
export var move_dir:Vector3 = Vector3.UP
export var move_dist:float = 1.0

var t:float = 0.0
onready var startPos:Vector3 = transform.origin

func _process(delta):
	t += delta
	transform = transform.rotated(rotate_dir, speed * delta)
	transform.origin = startPos + (move_dir * sin(t) * move_dist)
