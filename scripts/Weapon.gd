# 9/8/21 Daniel
#
# This class serves as the base class for all weapons
# allowing extensions.
#
# Each weapon must adhere to a couple of restrictions:
# 1. Animations are defined strictly
#     "Idle", "Walk", "Fire", "Reload", "Melee"

extends Spatial
class_name Weapon

var sparkScene = preload("res://prefabs/sparks.tscn")

export var fire_audio_path:NodePath
onready var fire_audio = get_node(fire_audio_path) as AudioStreamPlayer

var is_firing:bool = false

var fire_rate: int = 1
var max_ammo: int = 10
var ammo: int = 10
var max_range: float = 100.0

var spread:float = 1.0

var cooldown:float = 1.0
var counter:float = 0.0

func _process(delta):
	counter += delta

func fire(ray: RayCast):
	if ammo > 0 && counter >= cooldown:
		is_firing = true
		if ray.is_colliding():
			#Spawn sparks
			var sparks = sparkScene.instance()
			var pos = ray.get_collision_point()
			var rand = Vector3(
				rand_range(-spread,spread),
				rand_range(-spread,spread),
				rand_range(-spread,spread))
			var norm = rand * (Vector3(1,1,1) - ray.get_collision_normal()).normalized()
			pos += norm
			sparks.global_transform.origin = pos
			get_node("/root").add_child(sparks)
			_onFire()

			#Apply forces
			var collider = ray.get_collider()
			if collider is RigidBody:
				collider = collider as RigidBody
				collider.apply_impulse(collider.to_local(pos),-ray.get_collision_normal())
		counter = 0
		ammo -= 1
		EventBus.emit_signal("update_ammo",ammo)
		if !fire_audio.playing:
			fire_audio.play()
	elif ammo <= 0:
		cancelFire()

func cancelFire():
	fire_audio.stop()
	is_firing = false

func _onFire():
	printerr("DEFAULT _OnFire CALLED")
