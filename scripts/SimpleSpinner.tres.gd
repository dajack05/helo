extends Spatial

var t = 0.0
onready var startPos = transform.origin

func _process(delta):
	t += delta
	rotate_y(delta)
	transform.origin.y = startPos.y + sin(t)/4
