extends Control

func _ready():
	EventBus.connect("update_ammo",self,"_onAmmoChanged")

func _onAmmoChanged(newAmmo:int):
	$"MarginContainer/Ammo Counter".text = "Ammo:"+String(newAmmo)
