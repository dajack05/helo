extends Spatial

func _ready():
	$Particles.emitting = true

func _process(_delta):
	if !$Particles.emitting:
		queue_free()
